import React, { Component } from "react";

class RenderProps extends Component {
  constructor() {
    super();
    this.state = {};
  }
  render() {
    return (
      <div className="App">
        <h1>{this.props.name(true)}</h1>
      </div>
    );
  }
}

export default RenderProps;
