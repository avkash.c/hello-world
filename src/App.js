import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Greet from "./components/greet";
import Count from "./components/Count";
import PortalComp from "./components/portalComp";
import ErrorBoundary from "./components/ErrorBoundary";
import RenderProps from "./renderProps";
import { UserProvider } from "./userContext";
import PostList from "./http-component/postList";
import PostList1 from "./http-component/postList1";
import CounterComp from "./hooks/Hooks1";
import ChangeName from "./hooks/Hooks2";
import ArrayHooks from "./hooks/Hooks3";
import UseEffectHooks from "./hooks/Hooks4";
import MouseMoveToggle from "./hooks/Hooks5";
import UseReducerOne from "./hooks/useReducer1";
import ParentCallback from "./hooks/ParentCallback";
import UseMemoComp from "./hooks/useMemo";
import UseRefInterval from "./hooks/useRefInterval";
import CounterWithHook from "./hooks/CounterWithCustomHook";

export const UserContext = React.createContext();

function App() {
  const arr = { first: "Avkash", mid: "Parth", last: "Parita" };

  return (
    <div className="App">
      <CounterWithHook />
      {/* <UseRefInterval /> */}
      {/* <UseMemoComp /> */}
      {/* <ParentCallback /> */}
      {/* <UseReducerOne /> */}
      {/* <UserContext.Provider value="Avkash">
        <MouseMoveToggle />
      </UserContext.Provider> */}
      {/* <UseEffectHooks /> */}
      {/* <ArrayHooks /> */}
      {/* <ChangeName /> */}
      {/* <CounterComp /> */}
      {/* <PostList1 /> */}
      {/* <PostList /> */}
      {/* <UserProvider value={arr}>
        <PortalComp />
        <Count name="Avkash" />
      </UserProvider> */}
      {/* <RenderProps name={isLoggeedIn => (isLoggeedIn ? "Avkash" : "Guest")} /> */}
      {/* <Count name="Avkash" />  */}
      {/* <ErrorBoundary>
        <Greet name="Bruce" superHuman="Batman">
          <p>The Dark Knight</p>
        </Greet>
      </ErrorBoundary>
      <ErrorBoundary>
        <Greet name="Clark" superHuman="Superman">
          <button>Fly high</button>
        </Greet>
        <Greet name="Diana" superHuman="Jocker" />
      </ErrorBoundary> */}
    </div>
  );
}

export default App;
