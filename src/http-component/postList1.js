import React, { Component } from "react";
import axios from "axios";

class PostList1 extends Component {
  constructor() {
    super();

    this.state = {
      userId: "",
      title: "",
      body: ""
    };
  }

  changeHandler = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  clickHandler = e => {
    console.log(this.state);
    axios
      .post("https://jsonplaceholder.typicode.com/posts", this.state)
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err);
      });
    e.preventDefault();
  };

  render() {
    const { userId, title, body } = this.state;
    return (
      <div>
        <form>
          <p>
            <input
              type="text"
              name="userId"
              value={userId}
              onChange={this.changeHandler}
            />
          </p>
          <p>
            <input
              type="text"
              name="title"
              value={title}
              onChange={this.changeHandler}
            />
          </p>
          <p>
            <input
              type="text"
              name="body"
              value={body}
              onChange={this.changeHandler}
            />
          </p>
          <button onClick={this.clickHandler}>Submit</button>
        </form>
      </div>
    );
  }
}

export default PostList1;
