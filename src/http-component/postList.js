import React, { Component } from "react";
import axios from "axios";

class PostList extends Component {
  constructor() {
    super();
    this.state = {
      posts: [],
      errMsg: ""
    };
  }

  componentDidMount() {
    axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then(res => {
        console.log(res);
        this.setState({
          posts: res.data
        });
      })
      .catch(err => {
        this.setState({
          errMsg: `${err}`
        });
        console.log(err);
      });
  }

  render() {
    const { posts } = this.state;
    const { errMsg } = this.state;
    return (
      <div>
        <p>List of Post</p>
        {posts.map(post => (
          <div key={post.id}>{post.title}</div>
        ))}
        {errMsg ? <div>{errMsg}</div> : null}
      </div>
    );
  }
}

export default PostList;
