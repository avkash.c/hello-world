import React, { Component, useState } from "react";

const useCustomCounter = (initValue = 0, val = 1) => {
  const [count, setCount] = useState(initValue);

  const increase = () => {
    setCount(prev => prev + val);
  };

  const decrease = () => {
    setCount(prev => prev - val);
  };

  const reset = () => {
    setCount(initValue);
  };

  return [count, increase, decrease, reset];
};

export default useCustomCounter;
