import React, { Component } from "react";

const CountOne = ({ text, count }) => {
  console.log(`Rendering ${text}`);
  return (
    <h3>
      {text} - {count}
    </h3>
  );
};

export default React.memo(CountOne);
