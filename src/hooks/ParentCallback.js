import React, { Component, useState, useCallback } from "react";
import Title from "./title";
import CountOne from "./Count";
import Button from "./Button";

const ParentCallback = () => {
  const [age, setAge] = useState(25);
  const [salary, setSalary] = useState(50000);

  const incAge = useCallback(() => {
    setAge(age + 1);
  }, [age]);

  const incSalary = useCallback(() => {
    setSalary(salary + 1000);
  }, [salary]);
  return (
    <div>
      <Title />
      <CountOne text="Age" count={age} />
      <Button handleClick={incAge}>Increment Age</Button>
      <CountOne text="Salary" count={salary} />
      <Button handleClick={incSalary}>Increment Salary</Button>
    </div>
  );
};

export default ParentCallback;
