import React, { Component, useState, useRef, useEffect } from "react";

const UseRefInterval = () => {
  const [timer, setTimer] = useState(0);
  const interval = useRef(null);

  useEffect(() => {
    interval.current = setInterval(() => {
      setTimer(prev => prev + 1);
    }, 1000);
    return () => {
      clearInterval(interval.current);
    };
  }, []);

  return (
    <div>
      Timer {timer}
      <button onClick={() => clearInterval(interval.current)}>
        Stop timer
      </button>
    </div>
  );
};

export default UseRefInterval;
