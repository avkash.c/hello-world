import React, { useState, useEffect } from "react";

const MouseMoveComp = () => {
  const [x, setX] = useState(0);
  const [y, setY] = useState(0);

  const logMousePosition = e => {
    console.log("Mouse Move");
    setX(e.clientX);
    setY(e.clientY);
  };

  useEffect(() => {
    console.log("Event Listener set");
    window.addEventListener("mousemove", logMousePosition);

    return () => {
      console.log("Removed Mouse Listener");
      window.removeEventListener("mousemove", logMousePosition);
    };
  }, []);

  return (
    <div>
      Mouse Position X - {x} Y - {y}
    </div>
  );
};

export default MouseMoveComp;
