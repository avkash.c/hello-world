import React, { useState, useEffect, useContext } from "react";
import MouseMoveComp from "./ClassCompMouse";
import { UserContext } from "../App";

const MouseMoveToggle = () => {
  const [disp, setDisp] = useState(true);
  const user = useContext(UserContext);
  return (
    <div>
      <button onClick={() => setDisp(!disp)}>Toggle Listener {user}</button>
      {disp && <MouseMoveComp />}
    </div>
  );
};

export default MouseMoveToggle;
