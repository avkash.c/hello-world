import React, { Component, useState, useMemo } from "react";

const UseMemoComp = () => {
  const [counter1, setCounter1] = useState(0);
  const [counter2, setCounter2] = useState(0);

  const isEven = useMemo(() => {
    for (let i = 0; i < 999999999; i++);
    return counter1 % 2 == 0;
  }, [counter1]);
  return (
    <div>
      <button onClick={() => setCounter1(prevCnt => prevCnt + 1)}>
        Count {counter1}
      </button>
      {isEven ? " Even" : " Odd"}
      <br />
      <button onClick={() => setCounter2(prevCnt => prevCnt + 1)}>
        Count {counter2}
      </button>
    </div>
  );
};

export default UseMemoComp;
