import React, { useState } from "react";

const CounterComp = () => {
  const [count, setCount] = useState(0);
  return (
    <div>
      <button onClick={() => setCount(prevCount => prevCount + 1)}>
        Count {count}
      </button>
      <button onClick={() => setCount(prevCount => prevCount + 5)}>
        Count5 {count}
      </button>
    </div>
  );
};

export default CounterComp;
