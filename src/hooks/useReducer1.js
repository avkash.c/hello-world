import React, { useReducer } from "react";

const initState = { counter1: 0, counter2: 0 };
const reducer = (state, action) => {
  switch (action.type) {
    case "increase1":
      return { ...state, counter1: state.counter1 + 1 };
    case "decrease1":
      return { ...state, counter1: state.counter1 - 1 };
    case "reset1":
      return { ...state, counter1: initState.counter1 };
    case "increase2":
      return { ...state, counter2: state.counter2 + 1 };
    case "decrease2":
      return { ...state, counter2: state.counter2 - 1 };
    case "reset2":
      return { ...state, counter2: initState.counter2 };
  }
};

const UseReducerOne = () => {
  const [count, dispatch] = useReducer(reducer, initState);
  return (
    <div>
      Count {count.counter1}
      <button
        onClick={() => {
          dispatch({ type: "increase1" });
        }}
      >
        Increase
      </button>
      <button
        onClick={() => {
          dispatch({ type: "decrease1" });
        }}
      >
        Decrease
      </button>
      <button
        onClick={() => {
          dispatch({ type: "reset1" });
        }}
      >
        Reset
      </button>
      <div>
        Count {count.counter2}
        <button
          onClick={() => {
            dispatch({ type: "increase2" });
          }}
        >
          Increase
        </button>
        <button
          onClick={() => {
            dispatch({ type: "decrease2" });
          }}
        >
          Decrease
        </button>
        <button
          onClick={() => {
            dispatch({ type: "reset2" });
          }}
        >
          Reset
        </button>
      </div>
    </div>
  );
};

export default UseReducerOne;
