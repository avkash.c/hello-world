import React, { useState, useEffect } from "react";

const UseEffectHooks = () => {
  const [count, setCount] = useState(0);
  const [name, setName] = useState("");

  useEffect(() => {
    console.log("Value updated");
    document.title = `You clicked ${count} times`;
  }, [count]);
  return (
    <div>
      <input type="text" value={name} onChange={e => setName(e.target.value)} />
      <button
        onClick={() => {
          setCount(prevVal => prevVal + 1);
        }}
      >
        Count {count}
      </button>
    </div>
  );
};

export default UseEffectHooks;
