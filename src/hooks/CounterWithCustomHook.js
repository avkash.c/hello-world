import React, { Component } from "react";
import useCustomCounter from "./useCustomCounter";

const CounterWithHook = () => {
  const [count, increase, decrease, reset] = useCustomCounter();
  return (
    <div>
      <span>Count {count}</span>
      <button onClick={increase}>Increase</button>
      <button onClick={decrease}>Decrease</button>
      <button onClick={reset}>Reset</button>
    </div>
  );
};

export default CounterWithHook;
