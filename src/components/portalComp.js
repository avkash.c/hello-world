import React, { Component } from "react";
import ReactDOM from "react-dom";
import UpdatedComponent from "./counterHOC";

const portalComp = props => {
  return ReactDOM.createPortal(
    <div className="App">
      <h1 onClick={props.Increase}>This is portal {props.count}</h1>
    </div>,
    document.getElementById("portal-root")
  );
};

export default UpdatedComponent(portalComp);
