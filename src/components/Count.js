import React, { Component } from "react";
import Child from "./child";
import PureComp from "./pureComp";
import UpdatedComponent from "./counterHOC";

class Count extends Component {
  // IncreaseFive = () => {
  //   this.Increase();
  //   this.Increase();
  //   this.Increase();
  //   this.Increase();
  //   this.Increase();
  // };

  clickHandler = () => {
    this.refClassComp.current.focusInput();
  };

  changeUsernameHandler = event => {
    this.setState({
      username: event.target.value
    });
  };

  changeCommentHandler = event => {
    this.setState({
      comment: event.target.value
    });
  };

  changeTopicHandler = event => {
    this.setState({
      topic: event.target.value
    });
  };

  submitHandler = event => {
    console.log(
      `${this.state.username} ${this.state.comment} ${this.state.topic}`
    );
    event.preventDefault();
  };

  componentDidMount() {
    // setInterval(() => {
    //   this.setState({
    //     count: 0
    //   });
    // }, 2000);
  }

  render() {
    // const { username, comment, topic } = this.state;
    // console.log("In parent");
    return (
      <div>
        {/* <PureComp count={this.state.count} ref={this.refClassComp} /> */}
        {/* <button onClick={this.clickHandler}>Focus Input</button> */}
        <Child
          Increase={this.props.Increase}
          count={this.props.count}
          {...this.props}
        />
        {/* <form onSubmit={this.submitHandler}>
          <div>
            <label>Username </label>
            <input
              type="text"
              value={username}
              onChange={this.changeUsernameHandler}
            />
          </div>
          <div>
            <label>Comment </label>
            <textarea
              value={comment}
              onChange={this.changeCommentHandler}
            ></textarea>
          </div>
          <div>
            <label>Topic </label>
            <select value={topic} onChange={this.changeTopicHandler}>
              <option>React</option>
              <option>Angular</option>
              <option>Veu</option>
            </select>
          </div>
          <button type="submit">Submit Form</button>
        </form> */}
        <button onClick={this.props.Increase}>
          Increase {this.props.count}
        </button>
      </div>
    );
  }
}

export default UpdatedComponent(Count);
