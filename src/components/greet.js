import React, { Component } from "react";

class Message extends Component {
  constructor() {
    super();
    this.state = {
      message: "Welcome visitor"
    };
  }

  changeMessage() {
    this.setState({
      message: "Thank you for subscribing"
    });
  }

  render() {
    if (this.props.superHuman === "Jocker") {
      throw new Error("Not a Hero");
    }
    return (
      <div>
        <h1>
          {this.state.message} {this.props.superHuman}
        </h1>
        <button onClick={() => this.changeMessage()}>Subscribe</button>
      </div>
    );
  }
}

export default Message;
