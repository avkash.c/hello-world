import React, { Component } from "react";
import UpdatedComponent from "./counterHOC";
import { UserConsumer } from "../userContext";

class Child extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message: "Hello from child"
    };
  }

  // clickHandler = () => {
  //   alert(`${this.state.message} ${this.props.cnt}`);
  //   this.props.evnt();
  // };

  render() {
    console.log(this.props);
    return (
      <div>
        <button onClick={this.props.Increase}>
          Click in child {this.props.count} {this.props.name}
        </button>
        <br />
        <UserConsumer>
          {username => {
            return username.mid;
          }}
        </UserConsumer>
      </div>
    );
  }
}

export default Child;
