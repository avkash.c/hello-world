import React, { Component } from "react";

const UpdatedComponent = OriginalComponent => {
  class NewComponent extends Component {
    constructor(props) {
      super(props);
      this.refClassComp = React.createRef();
      this.state = {
        username: "",
        comment: "",
        topic: "React",
        count: 0
      };
    }

    Increase = () => {
      this.setState(prevState => ({
        count: prevState.count + 1
      }));
    };

    clickHandler = () => {
      this.refClassComp.current.focusInput();
    };

    changeUsernameHandler = event => {
      this.setState({
        username: event.target.value
      });
    };

    changeCommentHandler = event => {
      this.setState({
        comment: event.target.value
      });
    };

    changeTopicHandler = event => {
      this.setState({
        topic: event.target.value
      });
    };

    submitHandler = event => {
      console.log(
        `${this.state.username} ${this.state.comment} ${this.state.topic}`
      );
      event.preventDefault();
    };

    render() {
      return (
        <OriginalComponent
          count={this.state.count}
          Increase={this.Increase}
          {...this.props}
        />
      );
    }
  }
  return NewComponent;
};

export default UpdatedComponent;
