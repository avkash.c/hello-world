Points to refer:-

==> Context:-

    Passing value in UserProvider eg. <UserProvider value={any value} />,
    In  <UserConsumer>{(element)=>{return element.firstName } } </UserConsumer>
    //if value is obj and contain firstName

    Default value can be passed in React.createContext(<__HERE__>)

    2 methods,
        1. as shown above
        2. export default UserContext and to use value save the Class.contextType=UserContext.
            now use {this.context} for value

==> HTTP Get Post:-

    import axious from 'axios'

    get request with axios.get(<url>).then(...).catch(...)

    post request with axios.post(<url>,<data>).then(...).catch(...)

==>Hooks:-

    State of component in functional component (without class)
    Hooks don't work in classes

    import {useState} from 'react' and run useState() in top of functional component

    - useState() can be destructure in two array elements. 1st element is value (which is pass as parameter in useState()) and 2nd is method.
    eg. [count, setCount]  or  [subscribe, enableNotification]

    Call Hooks only at top level (Don't call hooks inside loops or nested function)

    useState doesn't update all objects. We have to pass every object value even when we want to update one value from object.

    - useEffect() runs on every render in functional(that can cause problem in large programs), likewise we use componentDidUpdate() in class component
    write function as a parameter in useEffect().

    In class component componentDidUpdate(prevProps, prevState) two parameters as shown. We can conditionaly run it according to the value of 'prevState', or else it will run every time when state is changed.
    eg. if(prevState.count != this.state.count)

    For useEffect() if we want to run conditionaly pass second parameter of array and pass the variable on which we want to render when changing that value of variable.
    eg. useEffect(function, [count]) --this will only render when count value is changed.
    eg. useEffect(function, []) --by passing empty array, it will run useEffect only once (Best way to create event listeners in functional components)

    If we want to clean eventListeners, we write code in componentWillUnmount() in class component, But in functional component, we can do the same by return from useEffect() function.
    eg. useEffect(()=>{
        //assign listener mouseMove
        return ()=>{
            //remove listener mouseMove
        }
    },[])

    - useContext()
    provide context same as class component with createContext() and export it (at top level)
    in child, import contex variables and pass them in useContext(UserContext) as parameter as shown.
    It will return the value of the context, so assign it in variable.

    - useReducer() works same ase useState()
    const [count, dispatch]=useReducer(reducer, initialState)
    it has two parameters and return two values as array.
    Here reducer is method and dispatch is also a method.
    eg.
    const initialState = 0;
    const reducer=(state, action)=>{
        switch(action){
            case 'value':
                return state+1;
            ...
        }
    }
    //and
    const [count, dispatch]=useReducer(reducer, initialState)
    //and
    onClick={()=>dispatch('increase')}

    Multiple useReducer can be use for handeling two different counter (or two different process).
    eg. Just create const [cnt, dsptch]=useReducer(reducer, initialState) to perform same process with different counter.

    useReducer with useContext to pass it in child branch functional component.

    between useReducer and useState, useReducer is better for much complex data handling, or changing two or more state properties.

    -useCallback() to optimise functional component rendering. For that optimize EVERY child component with React.memo()
    eg. export default React.memo(ComponentName)
    import {useCallback} from 'react'

    Use: const functionName=useCallback(<functionBody>,[variable])
    where variable is on which the function is depends on.

    - useMemo() also for optimization of functional component. Use same as useCallback().
    The difference is that useCallback caches the provided function instance itself, and useMemo invoke the provided function caches its result.
    So if you want to cache the function, use useCallback
    and if you want to cache the result of invoked function use useMemo

    - useRef()
    import and assign useRef(null) to variable
    and give that variable name as value of ref attribute in jsx part.
    eg. const inputRef = useRef(null)
    <input ref={inputRef} type="text">

    Also if we want to access variable inside of useEffect to the jsx part, we can use useRef().
    eg. useRefInterval.js file

    - Custome Hooks are created as new functional component starting with 'use' and without returning jsx (only return values).
    We can use other hooks components like useState or useEffect in custome hooks.
    Look at 'useCustomCounter.js' and it is used in 'CounterWithCustomHook.js'
